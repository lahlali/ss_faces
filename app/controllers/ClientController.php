<?php

class ClientController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return View::make('hello');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$client = new Clients;
		$client->email = Request::get('email');
		if (Input::file('image')->isValid())
		{
			$client->image = Input::file('image');
		}
		$client->save();
		return Response::json(array('error' => false), 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$clients = Clients::where('email', $id)
        	->take(1)
        	->get();
 
        return Response::json(array(
            'error' => false,
            'clients' => $clients->toArray()),
            200
        );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$client = Clients::where('email', $id)
        	->take(1)
        	->get();
        $cli=$client[0];

		$cli->email = Request::get('email');
		if (Input::file('image')->isValid())
		{
			$cli->image = Input::file('image');
		}
 
		$cli->save();
 
        return Response::json(array(
            'error' => false,
            'id'=> $cli,
            'message' => 'user updated'),
            200
        );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
